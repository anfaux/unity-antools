﻿/*
    - Adds menu item Tools/Armature/Assign

    - When invoked, examines the source tree and merges it by assigning it to be a child of the parent of the same named node.
      For instance if the source (outfit) armature has a "Belly" node, this is moved to become a child of the Avatar's main armature "Belly" node

*/
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace An
{
    public class ArmatureMerge : MonoBehaviour
    {
        private class PairOfTransforms
        {
            public Transform a;
            public Transform b;
            public PairOfTransforms(Transform a, Transform b)
            {
                this.a = a;
                this.b = b;
            }
        }

        private static List<PairOfTransforms> Operations = new List<PairOfTransforms>();

        public static void Merge(GameObject srcObj, GameObject destObj)
        {
            string srcPath = An.Util.GetGameObjectPath(srcObj);
            string destPath = An.Util.GetGameObjectPath(destObj);

            GameObject armatureSrc = GameObject.Find(srcPath);
            GameObject armatureDest = GameObject.Find(destPath);

            Operations.Clear();
            Traverse(armatureSrc.transform, 0, srcPath, destPath);

            foreach (PairOfTransforms operation in Operations)
            {
                operation.a.SetParent(operation.b);
            }
            //DestroyImmediate(srcObj);
        }

        public static void Traverse(Transform node, int depth, string path, string destPath)
        {
            foreach (Transform child in node)
            {
                string currentPath = path + "/" + child.name;
                string currentDestPath = destPath + "/" + child.name;

                var destination = GameObject.Find(currentDestPath);

                if (destination != null)
                {
                    var target = NodeFindChild(
                        child.name,
                         destination.transform
                     );

                    PairOfTransforms operation = new PairOfTransforms(
                        child,
                        GameObject.Find(currentDestPath).transform
                    );
                    Operations.Add(operation);
                }
                else
                {
                    Debug.Log("** SKIP: ** (Missing Destination) " + currentDestPath, null);
                }

                if (child.childCount > 0)
                {
                    Traverse(child, depth++, currentPath, currentDestPath);
                }
            }
        }

        public static GameObject NodeFindChild(string lookingFor, Transform t)
        {
            GameObject found = null;
            foreach (Transform child in t)
            {
                if (child.name == lookingFor)
                {
                    found = child.gameObject;
                    break;
                }
            }
            return found;
        }
    }
}
