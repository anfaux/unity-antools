﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;

namespace An
{
    public class ArmatureTagger : MonoBehaviour
    {
        public static void ExcludeFromBones(string tagName)
        {
            GameObject[] taggedObjects = GameObject.FindGameObjectsWithTag(tagName);

            GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
            foreach (GameObject go in allObjects)
            {
                if (go.activeInHierarchy)
                {
                    DynamicBone[] DynamicBoneComponents = go.GetComponents<DynamicBone>(); //UnityEngine.Object.FindObjectsOfType<DynamicBone>();

                    if (DynamicBoneComponents.Length > 0)
                    {
                        string path = An.Util.GetGameObjectPath(go);
                        Debug.Log(
                            path
                                + ": contains "
                                + DynamicBoneComponents.Length
                                + " Dynamic Bone Components!"
                        );
                        foreach (DynamicBone Component in DynamicBoneComponents)
                        {
                            foreach (GameObject objectToExclude in taggedObjects)
                            {
                                if (!Component.m_Exclusions.Contains(objectToExclude.transform))
                                    Component.m_Exclusions.Add(objectToExclude.transform);
                            }
                        }
                    }
                }
            }
            Debug.Log("Tagged Objects: " + taggedObjects.Length);
        }
        public static void UnExcludeFromBones(string tagName)
        {
            GameObject[] taggedObjects = GameObject.FindGameObjectsWithTag(tagName);
            GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
            foreach (GameObject go in allObjects)
            {
                if (go.activeInHierarchy)
                {
                    DynamicBone[] DynamicBoneComponents = go.GetComponents<DynamicBone>(); //UnityEngine.Object.FindObjectsOfType<DynamicBone>();

                    if (DynamicBoneComponents.Length > 0)
                    {
                        string path = An.Util.GetGameObjectPath(go);
                        foreach (DynamicBone Component in DynamicBoneComponents)
                        {
                            foreach (GameObject objectToExclude in taggedObjects)
                            {
                                Component.m_Exclusions.Remove(objectToExclude.transform);
                            }
                        }
                    }
                }
            }
        }
        public static void Tag(GameObject obj, string tagName)
        {
            string path = An.Util.GetGameObjectPath(obj);

            Util.AddTag(tagName);
            Traverse(
                obj.transform,
                0,
                path,
                (Transform child, string currentPath) =>
                {
                    child.transform.tag = tagName;
                }
            );
        }

        public static void Clear(GameObject obj)
        {
            string path = An.Util.GetGameObjectPath(obj);

            Traverse(
                obj.transform,
                0,
                path,
                (Transform child, string currentPath) =>
                {
                   child.transform.tag = "Untagged";
                }
            );
        }

        public static void Traverse(
            Transform node,
            int depth,
            string path,
            Action<Transform, string> action
        ) {
            foreach (Transform child in node)
            {
                string currentPath = path + "/" + child.name;
                action(child, currentPath);
                if (child.childCount > 0)
                {
                    Traverse(child, depth++, currentPath, action);
                }
            }
        }

        public static GameObject NodeFindChild(string lookingFor, Transform t)
        {
            GameObject found = null;
            foreach (Transform child in t)
            {
                if (child.name == lookingFor)
                {
                    found = child.gameObject;
                    break;
                }
            }
            return found;
        }
    }
}
