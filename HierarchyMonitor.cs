using System.Linq;
using UnityEditor;
using UnityEngine;
using An;

[InitializeOnLoadAttribute]


public class ExampleClass : MonoBehaviour
{
    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 1);
        Debug.Log(transform.name);
    }
}

public static class HierarchyMonitor
{

    static HierarchyMonitor()
    {
        EditorApplication.hierarchyChanged += OnHierarchyChanged;
        Debug.Log("Monitoring");
    }

    static void OnHierarchyChanged()
    {
        var all = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        var numberVisible =
            all.Where(obj => (obj.hideFlags & HideFlags.HideInHierarchy) != HideFlags.HideInHierarchy).Count();
        Debug.Log(An.Config.apiUrl);
        Debug.LogFormat("There are currently {0} GameObjects visible in the hierarchy.", numberVisible);
    }
}