using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System;

namespace An
{
    public static class Config{
        public static string  apiUrl = "http://localhost:3000";
    }
    

    public static class Util
    {
        public static string GetGameObjectPath(GameObject obj)
        {
            string path = "/" + obj.name;
            while (obj.transform.parent != null)
            {
                obj = obj.transform.parent.gameObject;
                path = "/" + obj.name + path;
            }
            return path;
        }

        public static int getBlendShapeIndex(GameObject obj, string targetName)
        {
            SkinnedMeshRenderer gameObj = obj.GetComponent<SkinnedMeshRenderer>();
            Mesh m = gameObj.sharedMesh;
            string[] arr;
            arr = new string[m.blendShapeCount];
            int index = -1;
            for (int i = 0; i < m.blendShapeCount; i++)
            {
                string blendShapeName = m.GetBlendShapeName(i);
                if (blendShapeName == targetName)
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public static string[] getBlendShapeNames(GameObject obj)
        {
            //https://stackoverflow.com/questions/22694672/get-blendshapes-by-name-rather-than-by-index
            SkinnedMeshRenderer gameObj = obj.GetComponent<SkinnedMeshRenderer>();
            Mesh m = gameObj.sharedMesh;
            string[] arr;
            arr = new string[m.blendShapeCount];
            for (int i = 0; i < m.blendShapeCount; i++)
            {
                string s = m.GetBlendShapeName(i);
                arr[i] = s;
            }
            return arr;
        }

        public static float getBlendShapeWeightByName(GameObject obj, string targetName)
        {
            SkinnedMeshRenderer gameObj = obj.GetComponent<SkinnedMeshRenderer>();
            Mesh m = gameObj.sharedMesh;
            string[] arr;
            arr = new string[m.blendShapeCount];
            for (int i = 0; i < m.blendShapeCount; i++)
            {
                string blendShapeName = m.GetBlendShapeName(i);
                if (blendShapeName == targetName)
                {
                    return gameObj.GetBlendShapeWeight(i);
                }
            }

            return -1;
        }

        public static void WriteToFile(string data, string path)
        {
            bool append = false;
            StreamWriter writer = new StreamWriter(path, append);
            writer.WriteLine(data);
            writer.Close();
            StreamReader reader = new StreamReader(path);
        }

        public static string ReadFromFile(string path)
        {
            StreamReader reader = new StreamReader(path);
            string data = reader.ReadToEnd();
            reader.Close();
            return data;
        }

        public static void AddTag(string tag)
        {
            //https://answers.unity.com/questions/33597/is-it-possible-to-create-a-tag-programmatically.html
            UnityEngine.Object[] asset = AssetDatabase.LoadAllAssetsAtPath(
                "ProjectSettings/TagManager.asset"
            );
            if ((asset != null) && (asset.Length > 0))
            {
                SerializedObject so = new SerializedObject(asset[0]);
                SerializedProperty tags = so.FindProperty("tags");

                for (int i = 0; i < tags.arraySize; ++i)
                {
                    if (tags.GetArrayElementAtIndex(i).stringValue == tag)
                    {
                        return; // Tag already present, nothing to do.
                    }
                }

                tags.InsertArrayElementAtIndex(0);
                tags.GetArrayElementAtIndex(0).stringValue = tag;
                so.ApplyModifiedProperties();
                so.Update();
            }
        }

        public static void DumpToConsole(object obj)
        {
            var output = JsonUtility.ToJson(obj, true);
            Debug.Log(output);
        }
    }
}

