using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;
using An;

public class AnTools : EditorWindow
{

    private static AnTools _winInstance;

    private static GameObject srcObj;
    private static GameObject destObj;

    private static GameObject armatureSrc;
    private static GameObject armatureDest;

    private static GameObject targetObj;
    private static string tagName = "NeverBone";

    private static GameObject autoAvatar;
    private static GameObject autoOutfit;

    bool s1,
        s2,
        s3,
        s4 = false;

    bool applyToChildren = false;

    [MenuItem("Tools/AnTools")]
    public static void ShowWindow()
    {
        _winInstance = GetWindow<AnTools>();
    }

    private void OnGUI()
    {
        // Blend Shape Values
        s1 = EditorGUILayout.BeginFoldoutHeaderGroup(s1, "BlendShape Values");
        if (s1)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Source");
            srcObj = EditorGUILayout.ObjectField(srcObj, typeof(GameObject), true) as GameObject;
            GUI.enabled = (srcObj != null);
            if (GUILayout.Button("Export"))
            {
                An.BlendShapeTransfer.ExportJson(srcObj, applyToChildren);
            }
            if (GUILayout.Button("Import"))
            {
                An.BlendShapeTransfer.ImportJson(srcObj);
            }
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Destination");
            destObj = EditorGUILayout.ObjectField(destObj, typeof(GameObject), true) as GameObject;
            GUI.enabled = (destObj != null);
            if (GUILayout.Button("Zero"))
            {
                An.BlendShapeCopy.Zero(destObj, applyToChildren);
            }
            if (GUILayout.Button("Export"))
            {
                An.BlendShapeTransfer.ExportJson(destObj, applyToChildren);
            }
            if (GUILayout.Button("Import"))
            {
                An.BlendShapeTransfer.ImportJson(destObj);
            }

            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            applyToChildren = GUILayout.Toggle(applyToChildren, "Apply to children");
            GUI.enabled = true;
            GUI.enabled = (srcObj != null && destObj != null && !(srcObj == destObj));
            if (GUILayout.Button("Apply"))
            {
                An.BlendShapeCopy.Copy(srcObj, destObj, applyToChildren);
            }
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        // Armature Merge
        s2 = EditorGUILayout.BeginFoldoutHeaderGroup(s2, "Armature Merge");
        if (s2)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Source");
            armatureSrc =
                EditorGUILayout.ObjectField(armatureSrc, typeof(GameObject), true) as GameObject;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Destination");
            armatureDest =
                EditorGUILayout.ObjectField(armatureDest, typeof(GameObject), true) as GameObject;
            GUI.enabled = (armatureDest != null);
            if (GUILayout.Button("Clean"))
            {
                if (
                    EditorUtility.DisplayDialog(
                        "Clean Armature?",
                        "This will delete any previously merged armature from"
                            + An.Util.GetGameObjectPath(armatureDest)
                            + ". Operation cannot be reversed!",
                        "Clean",
                        "Cancel"
                    )
                ) {
                    An.ArmatureClean.Clean(armatureDest);
                }
            }

            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUI.enabled = (armatureSrc != null);

            GUI.enabled = false;
            if (armatureSrc != null && armatureDest != null && !(armatureSrc == armatureDest))
            {
                if (
                    (PrefabUtility.GetPrefabAssetType(armatureSrc) == PrefabAssetType.NotAPrefab)
                    && (
                        PrefabUtility.GetPrefabAssetType(armatureDest) == PrefabAssetType.NotAPrefab
                    )
                ) {
                    GUI.enabled = true;
                }
            }
            if (GUILayout.Button("Merge"))
            {
                if (
                    EditorUtility.DisplayDialog(
                        "Merge armatures?",
                        "Merge from "
                            + An.Util.GetGameObjectPath(armatureSrc)
                            + " to "
                            + An.Util.GetGameObjectPath(armatureDest)
                            + ", deleting from "
                            + An.Util.GetGameObjectPath(armatureSrc)
                            + " ?",
                        "Merge",
                        "Cancel"
                    )
                ) {
                    An.ArmatureMerge.Merge(armatureSrc, armatureDest);
                }
            }
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        // Dynamic Bones
        s3 = EditorGUILayout.BeginFoldoutHeaderGroup(s3, "Dynamic Bone Tagger");
        if (s3)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Armature Root");
            targetObj =
                EditorGUILayout.ObjectField(targetObj, typeof(GameObject), true) as GameObject;

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            tagName = EditorGUILayout.TextField("Tag:", tagName);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUI.enabled = (targetObj != null);
            if (GUILayout.Button("Untag"))
            {
                if (
                    EditorUtility.DisplayDialog(
                        "Untag Armature?",
                        "This will clear tag from secondary armature at"
                            + An.Util.GetGameObjectPath(targetObj),
                        "Clear",
                        "Cancel"
                    )
                ) {
                    An.ArmatureTagger.Clear(targetObj);
                }
            }
            GUI.enabled = ((targetObj != null) && !(String.IsNullOrEmpty(tagName)));
            if (GUILayout.Button("Tag"))
            {
                if (
                    EditorUtility.DisplayDialog(
                        "Tag Armature?",
                        "This will apply "
                            + tagName
                            + " tag to"
                            + An.Util.GetGameObjectPath(targetObj),
                        "Tag",
                        "Cancel"
                    )
                ) {
                    An.ArmatureTagger.Tag(targetObj, tagName);
                }
            }
            GUI.enabled = true;

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUI.enabled = (!(String.IsNullOrEmpty(tagName)));

            if (GUILayout.Button("Unexclude"))
            {
                if (
                    EditorUtility.DisplayDialog(
                        "Remove Tagged Objects?",
                        "This will remove all game objects tagged "
                            + tagName
                            + " from the exclusion list of all DynamicBone components.",
                        "UnExclude",
                        "Cancel"
                    )
                ) {
                    An.ArmatureTagger.UnExcludeFromBones(tagName);
                }
            }
            if (GUILayout.Button("Exclude"))
            {
                if (
                    EditorUtility.DisplayDialog(
                        "Add Tagged Objects?",
                        "This will add all game objects tagged "
                            + tagName
                            + " to the exclusion list of all DynamicBone components.",
                        "Exclude",
                        "Cancel"
                    )
                ) {
                    An.ArmatureTagger.ExcludeFromBones(tagName);
                }
            }

            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        // Dynamic Bones
        s4 = EditorGUILayout.BeginFoldoutHeaderGroup(s4, "Fully Automated Luxury Communism");
        if (s4)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Avatar");
            autoAvatar =
                EditorGUILayout.ObjectField(autoAvatar, typeof(GameObject), true) as GameObject;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Outfit");
            autoOutfit =
                EditorGUILayout.ObjectField(autoOutfit, typeof(GameObject), true) as GameObject;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUI.enabled = (autoOutfit != null && autoAvatar != null);
            string neverBoneTag = "NeverBone";
            if (GUILayout.Button("Rig"))
            {
                GameObject srcArmature = autoOutfit.transform.Find("Armature").gameObject;
                GameObject destArmature = autoAvatar.transform.Find("Armature").gameObject;

                // Apply blend shapes to all children
                GameObject srcOfBlendShapes = autoAvatar.transform.Find("Body").gameObject;
                An.BlendShapeCopy.Copy(srcOfBlendShapes, autoOutfit, applyToChildren);

                // Merge the armatures
                An.ArmatureMerge.Merge(srcArmature, destArmature);

                // Tag the secondary armature
                An.ArmatureTagger.Tag(destArmature, neverBoneTag);

                // Add exclusions
                An.ArmatureTagger.ExcludeFromBones(neverBoneTag);

                // Move the outfit objects into Avatar
                autoOutfit.transform.name = "Outfit";
                autoOutfit.transform.SetParent(autoAvatar.transform);
            }

            GUI.enabled = (autoAvatar != null);
            if (GUILayout.Button("Remove"))
            {
                // Undo exclusions
                An.ArmatureTagger.UnExcludeFromBones(neverBoneTag);

                // Clean armature
                GameObject destArmature = autoAvatar.transform.Find("Armature").gameObject;
                An.ArmatureClean.Clean(destArmature);

                // Delete outfit
                DestroyImmediate(autoOutfit);
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndFoldoutHeaderGroup();
    }
}



